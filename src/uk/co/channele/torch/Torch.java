package uk.co.channele.torch;

import android.app.Activity;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import java.util.List;
import java.util.ListIterator;

public class Torch extends Activity implements OnClickListener {
    
    private Button switchOnOff;
    private Camera cam;
    private boolean lampOn = false;
    private Camera.Parameters camParams;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        switchOnOff = (Button)findViewById(R.id.on_off);
        switchOnOff.setOnClickListener(this);
    }
    
    /** Initialise camera */
    @Override
    public void onResume() {
        TextView msgPane;
        super.onResume();
        cam = Camera.open();
        camParams = cam.getParameters();
        if (camParams.getFlashMode() != Camera.Parameters.FLASH_MODE_TORCH) {
            switchLamp();
        }
    }
    
    public void onPause() {
        super.onPause();
        camParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        cam.setParameters(camParams);
        lampOn = false;
        cam.release();
    }
    
    /** Switch flash lamp to alternate state */
    private void switchLamp() {
        camParams = cam.getParameters();
        if (lampOn) {
            camParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        } else {
            camParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        }
        lampOn = !lampOn;
        cam.setParameters(camParams);
    }

        
    /** Handle buttons */
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.on_off:
            switchLamp();
        default:
            /* FIXME: trap this */
        }
    }
    
}
